#define _CRT_SECURE_NO_WARNINGS
#include<math.h>
#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<string.h>
#include<locale.h>
#include<Windows.h>

int simple(int num);
int findd(int f);
int evkl(int a, int b);
int finde(int f, int d);

int main() {
	//Метод шифрования RSA
	//srand(time(NULL));
	int p, q, n, f, d, e;
	int num;
	char str[256] = { 0 };
	char code[256] = { 0 }, encode[256] = { 0 };
	puts("Print down your text.");
	gets(str);
	puts("Print down p: ");
	scanf("%d", &p);
	while (simple(p) == -1) {
		puts("Your p is not simple, print down new p:");
		scanf("%d", &p);
	}
	puts("Print down q: ");
	scanf("%d", &q);
	while (simple(q) == -1 || q==p) {
		puts("Your q is not simple, print down new q:");
		scanf("%d", &q);
	}
	n = p*q;
	printf("n == %d.\n", n);
	f = (p - 1)*(q - 1);
	printf("f == %d.\n", f);
	/*d = findd(f);	
	printf("d == %d.\n", d);
	e = finde(f, d);	
	printf("e == %d.\n", e);*/
	puts("Print down d: ");
	scanf("%d", &d);
	while (evkl(d, f) != 1 || d == 1 || simple(d) != 1 || d >=f) {
		puts("Your d is wrong. Print down new d: ");
		scanf("%d", &d);
	}
	puts("Print down e: ");
	scanf("%d", &e);
	while ((e*d) % f != 1 || simple(e) != 1 || e == d || e >= f) {
		puts("Your e is wrong. Print down new e: ");
		scanf("%d", &e);
	}
	//coding
	printf("That's your code: ");
	for (int i = 0; i < strlen(str); i++)
	{
		num = str[i] - 'A' + 1;
		//printf("\nnum is %d \n", num);
		double a = fmod(pow(num, e), n);
		code[i] = a; 
		printf("%d ", (int)a);
		//printf("\ncode[i] is %c \n", code[i]);
	}
	printf("\n");
	//printf("That's your code: %s.\n", code);
	//encoding
	for (int j = 0; j < strlen(code); j++)
	{
		encode[j] = fmod(powe(code[j],d), n);
		encode[j] = encode[j] + 'A' - 1;
	}
	printf("That's the original string: %s.\n", encode);
	system("pause");
	return 0;
}

int simple(int num) {
	for (int i = 2; i < num; i++)
	{
		if (num%i == 0) return -1;
	}
	return 1;
}

int findd(int f) {
	int d;
	int k = 1;
	while (k != 0) {
		srand(time(NULL));
		d = 2 + rand() % (f - 2);
		if (evkl(d, f) == 1 && d != 1 && simple(d)==1) k = 0;
	}
	return d;
}

int evkl(int a, int b) {
	while (a!=0 && b!=0) {
		if (a > b) a = a%b;
		else b = b%a;
	}
	return abs(a+b);
}

int finde(int f, int d) {
	int e=1;
	int o = 1;
	while (o != 0) {
		e++;
		if ((e*d) % f == 1 && simple(e) == 1 && e != d && e<f) o = 0;
	} 
	return e;
}
